<?php
date_default_timezone_set('America/Sao_Paulo');
header('Content-Type: text/html; charset=UTF-8');

$pdo 	= new PDO("mysql:host=localhost;dbname=bdboleto","root","1");
$listar = $pdo->prepare("select *from livro");
$listar->execute();

$l = new ArrayIterator($listar->fetchAll(PDO::FETCH_OBJ));
while($l->valid()):

	echo $l->current()->preco."<br />";
	echo number_format($l->current()->preco,2,",",".")."<br />";
	echo '<a href="gerar_boleto.php">Pagar com boleto</a>' ."<br /> <br />";

	$l->next();
endwhile;
